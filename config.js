module.exports = {
  autodiscover: true,
  endpoint: process.env.CI_API_V4_URL,
  platform: 'gitlab',
  username: 'renovate-bot',
  labels: ["dependencies","renovate"],
};
